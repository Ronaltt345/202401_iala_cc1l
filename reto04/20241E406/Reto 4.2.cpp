#include <iostream>

using namespace std;

void printmatrix(int** matriz, const int n, const int m) {
    cout << "La matriz dinamica es de " << n << " por " << m << endl;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

void matrix(const int n, const int m) {
    int** matriz = new int* [n];
    for (int i = 0; i < n; ++i) {
        matriz[i] = new int[m];
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            matriz[i][j] = 0;
        }
    }

    printmatrix(matriz, n, m);

    for (int i = 0; i < n; ++i) {
        delete[] matriz[i];
    }

    delete[] matriz;
}

int main() {
    int numrows, numcols;

    cout << "Ingrese el numero de filas del arreglo dinamico:" << endl;
    cin >> numrows;
    cout << "Ingrese el numero de columnas del arreglo dinamico:" << endl;
    cin >> numcols;

    matrix(numrows, numcols);
    return 0;
}