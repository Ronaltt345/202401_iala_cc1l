#include <iostream>
#include <ctime>

using namespace std;

int num_aleatorio(int min, int max);
int* gen_arreglo(int& n);
void imprimir_arreglo();

int main() {
	srand(time(0));

	imprimir_arreglo();

	cout << "\n";
	system("pause");
	return 0;
}

int num_aleatorio(int min, int max) {
	return rand() % (max - min) + min;
}

int* gen_arreglo(int& n) {
	
	int* arreglo = new int[n];

		for (int i = 0; i < n; ++i) {
			arreglo[i] = num_aleatorio(1, 10001);
		}
	return arreglo;
}
void imprimir_arreglo() {
	int tama�o = num_aleatorio(100, 501);
	int* arreglo = gen_arreglo(tama�o);

	cout << "El tama�o del arreglo es: " << tama�o << "\n\n";

	for (int i = 0; i < tama�o; ++i) {
		cout << arreglo[i] << " ";
	}
	delete[] arreglo;
}