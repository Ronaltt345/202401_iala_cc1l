#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int* generarArreglo() {
    srand(time(0)); 

    int tamano = rand() % 401 + 100; 
    int* arreglo = new int[tamano];

    for (int i = 0; i < tamano; i++) {
        arreglo[i] = rand() % 10000 + 1; 
    }

    return arreglo;
}

int main() {
    int* arreglo = generarArreglo();

    
    std::cout << "Primeros 10 elementos: ";
    for (int i = 0; i < 10; i++) {
        cout << arreglo[i] << " ";
    }
        cout <<endl;

    delete[] arreglo; 
    system("pause");

    return 0;
}
