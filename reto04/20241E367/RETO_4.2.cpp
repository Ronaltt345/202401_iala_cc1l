#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int** crearMatriz(int n, int m) {
	int** matriz = new int* [n];
	for (int i = 0; i < n; i++) {
		matriz[i] = new int[i];
	}
	return matriz;
}

void imprimriMatriz(int** matriz, int n, int m) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << matriz[i][j] << " ";
		}
		cout << endl;
	}
}
void liberarMatriz(int** matriz, int n) {
	for (int i = 0; i < n; i++) {
		delete[] matriz[i];
	}
	delete[] matriz;
}
int main() {


	int n = 5;
	int m = 10;

	int** matriz = crearMatriz(n, m);

	imprimriMatriz(matriz, n, m);
	liberarMatriz(matriz, n);

	system("pause");
	return 0;
}